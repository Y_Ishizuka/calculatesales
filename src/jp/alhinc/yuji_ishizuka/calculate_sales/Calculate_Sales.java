package jp.alhinc.yuji_ishizuka.calculate_sales;

import java.io.*;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

class BranchInfo {
	private String branchCode;
	private String branchName;
	private int branchSalesSum;

	public BranchInfo() {
		branchCode = "";
		branchName = "";
		branchSalesSum = 0;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public int getBranchSalesSum() {
		return branchSalesSum;
	}
	public void addBranchSalesSum(int salesSum) throws OutOfSalesSumException {
		this.branchSalesSum += salesSum;
		//売上合計が10桁超えた例外処理
		if(0 > this.branchSalesSum || this.branchSalesSum > Integer.MAX_VALUE) {
			throw new OutOfSalesSumException("合計金額が10桁を超えました");
		}
	}
	public String printBranchInfo() {
		String info = (branchCode+","+branchName+","+branchSalesSum);
		return info;
	}
}

class CommodityInfo {
	private String commodityCode;
	private String commodityName;
	private int commoditySalesSum;

	public CommodityInfo() {
		commodityCode = "";
		commodityName = "";
		commoditySalesSum = 0;
	}
	public String getCommodityCode() {
		return commodityCode;
	}
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}
	public String getCommodityName() {
		return commodityName;
	}
	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}
	public int getCommoditySalesSum() {
		return commoditySalesSum;
	}
	public void addCommoditySalesSum(int commoditySales) throws OutOfSalesSumException {
		this.commoditySalesSum += commoditySales;
		if (0 > this.commoditySalesSum || this.commoditySalesSum > Integer.MAX_VALUE) {
			throw new OutOfSalesSumException("合計金額が10桁を超えました");
		}
	}
	public String printCommodityInfo() {
		String info = (commodityCode+","+commodityName+","+commoditySalesSum);
		return info;
	}
}

public class Calculate_Sales {
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Map<String,BranchInfo> branchList = new HashMap<String,BranchInfo>();
		Map<String,CommodityInfo> commodityList = new HashMap<String, CommodityInfo>();
		BranchInfo branch = new BranchInfo();
		CommodityInfo commodity = new CommodityInfo();
		String branchFileName = "branch.lst";
		String commodityFileName = "commodity.lst";
		String line, directoryPath;
		File directory = new File(args[0]);
		directoryPath = args[0];
		List<String> fileList = Arrays.asList(directory.list());
		BufferedReader br = null;
		LineNumberReader lr = null;
		BufferedWriter bw = null;
		Pattern p;
		Matcher m;

		try {
			//1.支店定義ファイル読み込み
			//ディレクトリ内の支店定義ファイル確認
			if(fileList.indexOf(branchFileName) == -1) {
				//ディレクトリ内にbranch.lstがない場合の例外処理
				throw new NotFoundFileException("支店定義ファイルが存在しません");
			}
			File branchesData = new File(directoryPath,branchFileName);
			br = new BufferedReader(new FileReader(branchesData));
			String[] wordList;
			while((line = br.readLine()) != null) {
				wordList = line.split(",");
				if(wordList[0].length() == 3) {
					branch.setBranchCode(wordList[0]);
				} else {
					//支店コードが3桁ではない例外処理
					throw new InvalidFormatOfItemFileException("支店定義ファイルのフォーマットが不正です");
				}
				if(!wordList[1].contains(",") && wordList[1].length() != 0) {
					branch.setBranchName(wordList[1]);
				} else {
					//支店名例外処理
					throw new InvalidFormatOfItemFileException("支店定義ファイルのフォーマットが不正です");
				}
				branchList.put(branch.getBranchCode(),branch);
				branch = new BranchInfo();
			}
			br.close();
			//商品定義ファイルの読み込み
			if(fileList.indexOf(commodityFileName) == -1) {
				//ディレクトリ内にcommodity.lstがない場合の例外処理
				throw new NotFoundFileException("商品定義ファイルが存在しません");
			}
			File commoditiesData = new File(directoryPath,commodityFileName);
			br = new BufferedReader(new FileReader(commoditiesData));
			while((line = br.readLine()) != null){
				wordList = line.split(",");
				p = Pattern.compile("^[0-9a-zA-Z]{8}$");
				m = p.matcher(wordList[0]);
				if(m.find()) {
					commodity.setCommodityCode(wordList[0]);
				} else {
					//商品コードが英数字8桁ではない場合の例外処理
					throw new InvalidFormatOfItemFileException("商品定義ファイルのフォーマットが不正です");
				}
				if(!wordList[1].contains(",") && wordList[1].length() != 0) {
					commodity.setCommodityName(wordList[1]);
				} else {
					//商品名例外処理
					throw new InvalidFormatOfItemFileException("商品定義ファイルのフォーマットが不正です");
				}
				commodityList.put(wordList[0], commodity);
				commodity = new CommodityInfo();
			}
			//branchListのKeyでソート
			Object[] mapKey = branchList.keySet().toArray();
			Arrays.sort(mapKey);

			//2.集計処理
			//rcdファイルの抽出
			ArrayList<String> rcdList = new ArrayList<String>();
			p = Pattern.compile("^\\d{8}\\.rcd$");
			for(String name : fileList) {
				m = p.matcher(name);
				if(m.find()) {
					rcdList.add(name);
				}
			}
			Collections.sort(rcdList);
			//rcdファイルの連番確認
			String[] check;
			int currentNumber;
			for(int i=0; i<rcdList.size(); i++) {
				check = rcdList.get(i).split("\\.");
				currentNumber = Integer.parseInt(check[0]);
				if(currentNumber != (i+1)) {
					//rcdファイルが連番ではない例外処理
					throw new InvalidFileOrderException("売上ファイルが連番になっていません");
				}
			}
			//rcdファイル読み込み、集計
			for(String rcd : rcdList) {
				File rcdData = new File(directoryPath,rcd);
				String branchCode = "", sales = "";
				int lineNumber;
				lr = new LineNumberReader(new FileReader(rcdData));
				while((line = lr.readLine()) != null) {
					lineNumber = lr.getLineNumber();
					if(lineNumber == 1) {
						if(branchList.containsKey(line)) {
							branchCode = line;
						} else {
							//支店コードに該当がない例外処理
							throw new NotFoundItemCodeException(rcd+"の支店コードが不正です");
						}
					} else if(lineNumber == 2) {
						if (commodityList.containsKey(line)) {
							sales = lr.readLine();
							branchList.get(branchCode).addBranchSalesSum(Integer.parseInt(sales));
							commodityList.get(line).addCommoditySalesSum(Integer.parseInt(sales));
						} else {
							//商品コードに該当がない例外処理
							throw new NotFoundItemCodeException(rcd+"の商品コードが不正です");
						}
					} else {
						//売上ファイルが4桁以上の例外処理
						throw new InvalidFormatOfSaleFileException(rcd+"のフォーマットが不正です");
					}
				}
				lr.close();
			}

			//3.集計結果出力
			//branchListを金額順にソート
			List<Map.Entry<String, BranchInfo>> sortedBranchList =
					new ArrayList<Map.Entry<String, BranchInfo>>(branchList.entrySet());
			Collections.sort(sortedBranchList, new Comparator<Map.Entry<String, BranchInfo>>() {
				public int compare(Entry<String, BranchInfo> entry1, Entry<String, BranchInfo> entry2) {
					return ((Integer)entry2.getValue().getBranchSalesSum()).compareTo((Integer)entry1.getValue().getBranchSalesSum());
				}
			});
			//commodityListを金額順位にソート
			List<Map.Entry<String, CommodityInfo>> sortedCommodityList =
					new ArrayList<Map.Entry<String, CommodityInfo>>(commodityList.entrySet());
			Collections.sort(sortedCommodityList, new Comparator<Map.Entry<String, CommodityInfo>>() {
				public int compare(Entry<String, CommodityInfo> entry1, Entry<String, CommodityInfo> entry2) {
					return ((Integer)entry2.getValue().getCommoditySalesSum()).compareTo((Integer)entry1.getValue().getCommoditySalesSum());
				}
			});
			//店舗別集計ファイル書き込み
			File outputBranhData = new File(directoryPath,"branch.out");
			bw = new BufferedWriter(new FileWriter(outputBranhData));
			for(Entry<String, BranchInfo> e : sortedBranchList) {
				bw.write(e.getValue().printBranchInfo());
				bw.newLine();
			}
			bw.close();
			//商品別集計ファイル書き込み
			File outputCommodityData = new File(directoryPath,"commodity.out");
			bw = new BufferedWriter(new FileWriter(outputCommodityData));
			for(Entry<String, CommodityInfo> e : sortedCommodityList) {
				bw.write(e.getValue().printCommodityInfo());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatOfItemFileException e) {
			System.out.println(e);
		} catch (NotFoundFileException e) {
			System.out.println(e);
		} catch (InvalidFileOrderException e) {
			System.out.println(e);
		} catch (OutOfSalesSumException e) {
			System.out.println(e);
		} catch (NotFoundItemCodeException e) {
			System.out.println(e);
		} catch (InvalidFormatOfSaleFileException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			try {
				if(br != null) {
					br.close();
				}
				if(lr != null) {
					lr.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}
	}

}

class NotFoundFileException extends Exception {
	private static final long serialVersionUID = 0001L;
	NotFoundFileException(String message){
		super(message);
	}
}

class InvalidFormatOfItemFileException extends Exception {
	private static final long serialVersionUID = 0002L;
	InvalidFormatOfItemFileException(String message){
		super(message);
	}
}

class InvalidFileOrderException extends Exception {
	private static final long serialVersionUID = 0003L;
	InvalidFileOrderException(String message){
		super(message);
	}
}

class OutOfSalesSumException extends Exception {
	private static final long serialVersionUID = 0004L;
	OutOfSalesSumException(String message){
		super(message);
	}
}

class NotFoundItemCodeException extends Exception {
	private static final long serialVersionUID = 0005L;
	NotFoundItemCodeException(String message){
		super(message);
	}
}

class InvalidFormatOfSaleFileException extends Exception {
	private static final long serialVersionUID = 0006L;
	InvalidFormatOfSaleFileException(String message){
		super(message);
	}
}
